/*****************************************************************************
 *  Created by Marcus Zy on 25/04/2012.                                       *
 *  Copyright (c) 2010 - 2012 zyxar.com                                       *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated documentation files (the "Software"), *
 * to deal in the Software without restriction, including without limitation  *
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,   *
 * and/or sell copies of the Software, and to permit persons to whom the      *
 * Software is furnished to do so, subject to the following conditions:       *
 *                                                                            *
 * The above copyright notice and this permission notice shall be included    *
 * in all copies or substantial portions of the Software.                     *
 *                                                                            *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS    *
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF                 *
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.     *
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY       *
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,       *
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE          *
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                     *
 ******************************************************************************/

package qqwry

import (
    "net"
)

var buffer []byte

func init() {
    buffer = make([]byte, 4)
}

type helper interface {
    ReadAt(b []byte, off int64) (n int, err error)
}

func read1byte(file helper, offset int64) byte {
    file.ReadAt(buffer, offset)
    return buffer[0]
}

func read2byte(file helper, offset int64) (ret int64) {
    file.ReadAt(buffer, offset)
    ret = int64(buffer[0]) | int64(buffer[1])<<8
    return
}

func read3byte(file helper, offset int64) (ret int64) {
    file.ReadAt(buffer, offset)
    ret = int64(buffer[0]) | int64(buffer[1])<<8 | int64(buffer[2])<<16
    return
}

func read4byte(file helper, offset int64) (ret int64) {
    file.ReadAt(buffer, offset)
    ret = int64(buffer[0]) | int64(buffer[1])<<8 | int64(buffer[2])<<16 | int64(buffer[3])<<24
    return
}

func readbytes(ip net.IP) int64 {
    v := ip.To4()
    return int64(v[0])<<24 | int64(v[1])<<16 | int64(v[2])<<8 | int64(v[3])
}

func readslice(v int64) net.IP {
    return net.IPv4(byte((v&0xFF000000)>>24), byte((v&0xFF0000)>>16), byte((v&0xFF00)>>8), byte(v&0xFF))
}
