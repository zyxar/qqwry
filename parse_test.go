package qqwry

import (
	"bytes"
	"fmt"
	"testing"
)

func TestParse(t *testing.T) {
	reader, err := NewReader("/opt/share/QQWry.dat")
	if err != nil {
		t.Fatal(err)
	}
	defer reader.Destroy()
	var i, j int64
	var ss string
	err = func() error {
		i, j = parseString("131.2.*.4")
		ss = fmt.Sprintf("%X %X", i, j)
		if bytes.Compare([]byte(ss), []byte("83020004 FF00")) != 0 {
			return fmt.Errorf("Parse IP address failed: %s\n", ss)
		}
		i, j = parseString("8")
		ss = fmt.Sprintf("%X %X", i, j)
		if bytes.Compare([]byte(ss), []byte("8000000 FFFFFF")) != 0 {
			return fmt.Errorf("Parse IP address failed: %s\n", ss)
		}
		i, j = parseString("8.")
		ss = fmt.Sprintf("%X %X", i, j)
		if bytes.Compare([]byte(ss), []byte("8000000 FFFF")) != 0 {
			return fmt.Errorf("Parse IP address failed: %s\n", ss)
		}
		i, j = parseString("8.*")
		ss = fmt.Sprintf("%X %X", i, j)
		if bytes.Compare([]byte(ss), []byte("8000000 FFFFFF")) != 0 {
			return fmt.Errorf("Parse IP address failed: %s\n", ss)
		}
		i, j = parseString("8.888")
		ss = fmt.Sprintf("%X %X", i, j)
		if bytes.Compare([]byte(ss), []byte("8FF0000 FFFF")) != 0 {
			return fmt.Errorf("Parse IP address failed: %s\n", ss)
		}
		i, j = parseString("8.a8")
		ss = fmt.Sprintf("%X %X", i, j)
		if bytes.Compare([]byte(ss), []byte("-1 0")) != 0 {
			return fmt.Errorf("Parse IP address failed: %s\n", ss)
		}
		i, j = parseString("8.8.a.a")
		ss = fmt.Sprintf("%X %X", i, j)
		if bytes.Compare([]byte(ss), []byte("-1 0")) != 0 {
			return fmt.Errorf("Parse IP address failed: %s\n", ss)
		}
		i, j = parseString("8.8.8.8a")
		ss = fmt.Sprintf("%X %X", i, j)
		if bytes.Compare([]byte(ss), []byte("8080808 0")) != 0 {
			return fmt.Errorf("Parse IP address failed: %s\n", ss)
		}
		i, j = parseString("8.8.8.8.abc")
		ss = fmt.Sprintf("%X %X", i, j)
		if bytes.Compare([]byte(ss), []byte("8080808 0")) != 0 {
			return fmt.Errorf("Parse IP address failed: %s\n", ss)
		}
		return nil
	}()
	if err != nil {
		t.Errorf("%v", err)
	}
}
