/*****************************************************************************
 *  Created by Marcus Zy on 25/04/2012.                                       *
 *  Copyright (c) 2010 - 2012 zyxar.com                                       *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated documentation files (the "Software"), *
 * to deal in the Software without restriction, including without limitation  *
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,   *
 * and/or sell copies of the Software, and to permit persons to whom the      *
 * Software is furnished to do so, subject to the following conditions:       *
 *                                                                            *
 * The above copyright notice and this permission notice shall be included    *
 * in all copies or substantial portions of the Software.                     *
 *                                                                            *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS    *
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF                 *
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.     *
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY       *
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,       *
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE          *
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                     *
 ******************************************************************************/

package qqwry

func parseString(addr string) (int64, int64) {
    return parse([]byte(addr))
}

func parse(addr []byte) (int64, int64) {
    var t, k uint32
    var ret, mask int64
    i := 0
    for j := 0; j < len(addr) && i < 4; j++ {
        if addr[j] == '.' {
            i++
            if i < 4 {
                ret = (ret << 8) + int64(t)
                t = 0
            }
        } else if addr[j] >= '0' && addr[j] <= '9' {
            if i < 4 {
                k = t*10 + uint32(addr[j]-0x30)
                if k < 256 {
                    t = k
                } else {
                    t = 0xFF
                }
            }
        } else if addr[j] == '*' {
            t = 0
            if i < 4 {
                mask |= int64(0xff << uint((3-i)*8))
            }
        } else {
            if ret < 0x010000 {
                return -1, 0
            }
            continue
        }
    }
    ret = (ret << 8) + int64(t)
    if i < 3 {
        ret = ret << uint(8*(3-i))
        mask |= int64((0x01 << uint(8*(3-i))) - 1)
    }
    return ret, mask
}
