/*****************************************************************************
 *  Created by Marcus Zy on 24/04/2012.                                       *
 *  Copyright (c) 2010 - 2012 zyxar.com                                       *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated documentation files (the "Software"), *
 * to deal in the Software without restriction, including without limitation  *
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,   *
 * and/or sell copies of the Software, and to permit persons to whom the      *
 * Software is furnished to do so, subject to the following conditions:       *
 *                                                                            *
 * The above copyright notice and this permission notice shall be included    *
 * in all copies or substantial portions of the Software.                     *
 *                                                                            *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS    *
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF                 *
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.     *
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY       *
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,       *
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE          *
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                     *
 ******************************************************************************/

package qqwry

import (
	"bytes"
	"fmt"
	iconv "github.com/zyxar/go-iconv"
	"io"
	"net"
	"os"
	"strings"
)

const (
	_REDIRECT_MODE_1 = byte(0x01)
	_REDIRECT_MODE_2 = byte(0x02)
)

var locale string

type qFile struct {
	*os.File
	head struct {
		first int64
		last  int64
	}
	buffer []byte
}

type QReader qFile

type Field struct {
	start, end    net.IP
	country, area string
}

type QFields []*Field

func NewReader(path string) (*QReader, error) {
	file, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	inf := int64(read4byte(file, 0))
	inl := int64(read4byte(file, 4))
	// verify()
	ret := QReader{file, struct{ first, last int64 }{inf, inl}, make([]byte, 512)}
	return &ret, nil
}

func init() { // initialized to read locale
	l := os.Getenv("LANG")
	n := strings.Split(l, ".")
	locale = n[1]
	if locale == "" { // default to utf8
		locale = "UTF-8"
	}
}

func (rd *QReader) Destroy() {
	rd.Close()
}

func (f *Field) String() string {
	return fmt.Sprintf("%-16v %-16v %s %s", f.start, f.end, f.country, f.area)
}

func (qf QFields) String() string {
	var s string
	for _, v := range qf {
		s += fmt.Sprintf("%-16v %-16v %s %s\n", v.start, v.end, v.country, v.area)
	}
	return s
}

func (rd *QReader) String() string {
	return fmt.Sprintf("header:\t%08X\nnumber:\t%d", rd.head, (rd.head.last-rd.head.first)/7) +
		fmt.Sprintf("\n%v", rd.Version())
}

func (rd *QReader) Version() (s string) {
	buffer := make([]byte, 32)
	n, err := rd.ReadAt(buffer, read3byte(rd, rd.head.last+4))
	if err != nil && err != io.EOF {
		return ""
	}
	s, err = iconv.Conv(string(buffer[4:n]), locale, "GBK")
	if err != nil {
		return ""
	}
	return
}

func (rd *QReader) query(h, t int64) (QFields, error) {
	var ret QFields
	var first, last, current int64
	addr := h
	for addr <= t {
		first = rd.head.first
		last = rd.head.last
		current = ((last-first)/7/2)*7 + first
		for current > first {
			if read4byte(rd, current) > addr {
				last = current
				current = ((last-first)/7/2)*7 + first
			} else {
				first = current
				current = ((last-first)/7/2)*7 + first
			}
		}
		field := new(Field)
		start := read4byte(rd, current)
		offset := read3byte(rd, current+4)
		end := read4byte(rd, offset)
		field.start = readslice(start)
		field.end = readslice(end)
		if addr > end {
			field.country = "UNKNOWN"
			ret = append(ret, field)
			break
		}
		offset += 4
		mode := read1byte(rd, offset)
		country := make([]byte, 256)
		var area []byte
		n := 0
		if mode == _REDIRECT_MODE_1 {
			offset = read3byte(rd, offset+1)
			if read1byte(rd, offset) == _REDIRECT_MODE_2 {
				off := read3byte(rd, offset+1)
				rd.ReadAt(country, off)
				n = bytes.IndexByte(country, 0x00)
				country = country[:n]
				offset += 4
			} else {
				rd.ReadAt(country, offset)
				n = bytes.IndexByte(country, 0x00)
				country = country[:n]
				offset += int64(n + 1)
			}
		} else if mode == _REDIRECT_MODE_2 {
			off := read3byte(rd, offset+1)
			rd.ReadAt(country, off)
			n = bytes.IndexByte(country, 0x00)
			country = country[:n]
			offset += 4
		} else {
			rd.ReadAt(country, offset)
			n = bytes.IndexByte(country, 0x00)
			country = country[:n]
			offset += int64(n + 1)
		}

		mode = read1byte(rd, offset)
		if mode == _REDIRECT_MODE_1 || mode == _REDIRECT_MODE_2 {
			offset = read3byte(rd, offset+1)
		}
		if offset != 0 {
			area = make([]byte, 256)
			rd.ReadAt(area, offset)
			n = bytes.IndexByte(area, 0x00)
			area = area[:n]
		}
		cz88 := []byte("CZ88.NET")
		if bytes.Compare(country[1:], cz88) != 0 {
			field.country, _ = iconv.Conv(string(country), locale, "GBK")
		}
		if len(area) > 1 && bytes.Compare(area[1:], cz88) != 0 {
			field.area, _ = iconv.Conv(string(area), locale, "GBK")
		}
		ret = append(ret, field)
		addr = end + 1
	}
	return ret, nil
}

func (rd *QReader) Query(s string) (QFields, error) {
	addr, mask := parseString(s)
	return rd.query(addr, addr|mask)
}

func verify(rd *QReader) bool { // TODO: verify file type in factory method
	return true
}

func (rd *QReader) Dump(path string) error {
	of, err := os.Create(path)
	if err != nil {
		return err
	}
	defer of.Close()
	return rd.dquery(of, int64(0x00000000), int64(0xFFFF0000))
}

func (rd *QReader) Queryx(s string) error {
	addr, mask := parseString(s)
	return rd.dquery(os.Stdout, addr, addr|mask)
}

func (rd *QReader) dquery(of *os.File, ss, tt int64) error {
	var first, last, current int64
	var start, end, offset int64
	var rcountry, rarea string
	country := make([]byte, 64)
	area := make([]byte, 256)
	addr := ss
	var mode byte
	var n int
	for addr <= tt {
		rcountry, rarea = "", ""
		first = rd.head.first
		last = rd.head.last
		current = ((last-first)/7/2)*7 + first
		for current > first {
			if read4byte(rd, current) > addr {
				last = current
				current = ((last-first)/7/2)*7 + first
			} else {
				first = current
				current = ((last-first)/7/2)*7 + first
			}
		}
		start = read4byte(rd, current)
		offset = read3byte(rd, current+4)
		end = read4byte(rd, offset)
		if addr > end {
			rcountry = "UNKNOWN"
			break
		}
		offset += 4
		country = country[:64]
		area = area[:256]
		mode = read1byte(rd, offset)
		if mode == _REDIRECT_MODE_1 {
			offset = read3byte(rd, offset+1)
			if read1byte(rd, offset) == _REDIRECT_MODE_2 {
				rd.ReadAt(country, read3byte(rd, offset+1))
				n = bytes.IndexByte(country, 0x00)
				country = country[:n]
				offset += 4
			} else {
				rd.ReadAt(country, offset)
				n = bytes.IndexByte(country, 0x00)
				country = country[:n]
				offset += int64(n + 1)
			}
		} else if mode == _REDIRECT_MODE_2 {
			rd.ReadAt(country, read3byte(rd, offset+1))
			n = bytes.IndexByte(country, 0x00)
			country = country[:n]
			offset += 4
		} else {
			rd.ReadAt(country, offset)
			n = bytes.IndexByte(country, 0x00)
			country = country[:n]
			offset += int64(n + 1)
		}

		mode = read1byte(rd, offset)
		if mode == _REDIRECT_MODE_1 || mode == _REDIRECT_MODE_2 {
			offset = read3byte(rd, offset+1)
		}
		if offset != 0 {
			rd.ReadAt(area, offset)
			n = bytes.IndexByte(area, 0x00)
			area = area[:n]
		}
		cz88 := []byte("CZ88.NET")
		if bytes.Compare(country[1:], cz88) != 0 {
			rcountry, _ = iconv.Conv(string(country), locale, "GBK")
		}
		if len(area) > 1 && bytes.Compare(area[1:], cz88) != 0 {
			rarea, _ = iconv.Conv(string(area), locale, "GBK")
		}
		addr = end + 1
		fmt.Fprintf(of, "%-16s %-16s %s %s\n", net.IPv4(byte(start>>24), byte(start>>16), byte(start>>8), byte(start)), net.IPv4(byte(end>>24), byte(end>>16), byte(end>>8), byte(end)), rcountry, rarea)
	}
	return nil
}

func (rd *QReader) Write() {

}
